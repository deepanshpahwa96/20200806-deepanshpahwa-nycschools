package com.example.educatenyc.View;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.educatenyc.Model.SchoolData;
import com.example.educatenyc.R;
import com.example.educatenyc.Utility.Utils;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


public class SchoolDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.schooldetail_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {

            SchoolData schoolData = (SchoolData) getIntent().getSerializableExtra(SchoolDetailFragment.ARG);
            if (schoolData != null) {
                toolbar.setTitle(schoolData.getSchoolName());
            }
            Bundle arguments = new Bundle();
            arguments.putSerializable(SchoolDetailFragment.ARG,
                    schoolData);
            /**Creating the Fragment and loading it into the container*/
            SchoolDetailFragment fragment = new SchoolDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.school_detail_container, fragment)
                    .commit();

        }
    }
    /**Overriding toolbar back button*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            Utils.logdata("OVERRIDE");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}