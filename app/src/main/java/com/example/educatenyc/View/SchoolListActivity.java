package com.example.educatenyc.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.educatenyc.Model.SATData;
import com.example.educatenyc.Model.SchoolData;
import com.example.educatenyc.Network.NYCSchoolApi;
import com.example.educatenyc.R;
import com.example.educatenyc.Utility.Utils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SchoolListActivity extends AppCompatActivity {

    private static final String TAG = "SchoolListActivity";
    private boolean mTwoPane;
    private Context context;
    private Realm realm;

    private RealmList<SchoolData> schoolDataList;
    private RealmList<SATData> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);

        /**Toolbar Setup*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.schoollist_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        context = this.getBaseContext();//TODO maybe prob

        /**Realm DB Setup*/
        Realm.init(SchoolListActivity.this);
        Utils.CONFIG = new RealmConfiguration.Builder()
//                            .name("school.data")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(Utils.CONFIG);

        if (findViewById(R.id.school_detail_container) != null) {
            mTwoPane = true;
        }

        RecyclerView schoolListRecyclerView = findViewById(R.id.rv_schoollist_schools);
        LinearLayout errorLayout = findViewById(R.id.ll_schoollist_error_layout);
        TextView errorMessage = findViewById(R.id.tv_schoollist_error_message);
        SearchView searchView = findViewById(R.id.sv_schoollist_search_bar);

        assert schoolListRecyclerView != null;
        setupRecyclerView((RecyclerView) schoolListRecyclerView);

        searchView.setOnClickListener(v -> {
            searchView.setIconified(false);
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onQueryFromSearchBar(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                onQueryFromSearchBar(query);
                return true;
            }

            private void onQueryFromSearchBar(String query) {
                /**
                 * This method filters the data set according to the query. If the query is empty, all the
                 * schools are shown. If no schools are found for the query, error message is displayed.
                 */

                if (query.isEmpty()) {
                    showRecyclerView(schoolListRecyclerView, errorLayout);
                    populateRecyclerView(schoolDataList, schoolListRecyclerView);
                } else {
                    RealmList<SchoolData> list = filter(query);
                    populateRecyclerView(list, schoolListRecyclerView);
                    if (list.isEmpty()) {
                        hideRecyclerView(schoolListRecyclerView, errorLayout, errorMessage, query);
//                        textView.setText(getResources().getString(R.string.empty_result_list_text) + " " + query + ".");
                    } else {
                        showRecyclerView(schoolListRecyclerView, errorLayout);
                    }
                }
            }
        });
    }

    /**This method is used by the SearchView to filter results containing entered query*/
    private RealmList<SchoolData> filter(String query) {
        realm = Realm.getInstance(Utils.CONFIG);
        RealmList<SchoolData> filteredSchoolList = new RealmList<>();
        RealmResults<SchoolData> queryResults = realm.where(SchoolData.class).contains(SchoolData.SCHOOL_NAME, query).findAll();
        filteredSchoolList.addAll(queryResults.subList(0,queryResults.size()));
        return filteredSchoolList;
    }
    /**Method to hide recycler view and show linear layout containing error message*/
    private void hideRecyclerView(RecyclerView recyclerView, LinearLayout linearLayout, TextView errorMessage, String query) {
        recyclerView.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
        errorMessage.setText(String.format("%s%s", getResources().getString(R.string.empty_list_text), query));
    }

    /**Method to show recycler view and hide linear layout containing error message*/
    private void showRecyclerView(RecyclerView recyclerView, LinearLayout linearLayout) {
        recyclerView.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.getBaseContext().getString(R.string.BASE_URL)) //TODO best place to put base url?
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()//TODO what is this for? remove?
                .build();

        NYCSchoolApi nycSchoolApi = retrofit.create(NYCSchoolApi.class);

        /**Network Call to fetch list of schools in NYC*/
        Call<RealmList<SchoolData>> schoolListNetworkCall = nycSchoolApi.getListOfSchools();
        schoolListNetworkCall.enqueue(new Callback<RealmList<SchoolData>>() {
            @Override
            public void onResponse(Call<RealmList<SchoolData>> call, Response<RealmList<SchoolData>> response) {
                if (response.isSuccessful()) {
                    Utils.logdata("schoolListNetworkCall successful");
                    schoolDataList = response.body();

                    realm = Realm.getInstance(Utils.CONFIG);
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(schoolDataList);
                    realm.commitTransaction();
                    realm.close();

                    populateRecyclerView(schoolDataList, recyclerView);
                } else {
                    //TODO toast
                    Utils.makeNetworkErrorToast(SchoolListActivity.this);
                    Utils.logdata("schoolListNetworkCall Failed");
                }
            }

            @Override
            public void onFailure(Call<RealmList<SchoolData>> call, Throwable t) {
                Utils.makeNetworkErrorToast(SchoolListActivity.this);
                Utils.logdata("schoolListNetworkCall Failed");
            }
        });

        /**Network Call to fetch performance data for schools in NYC*/
        Call<RealmList<SATData>> SATScoreNetworkCall = nycSchoolApi.getSchoolsScores();
        SATScoreNetworkCall.enqueue(new Callback<RealmList<SATData>>() {
            @Override
            public void onResponse(Call<RealmList<SATData>> call, Response<RealmList<SATData>> response) {
                if (response.isSuccessful()) {
                    Utils.logdata("SATScoreNetworkCall Successful");
                    object = response.body();

                    realm = Realm.getInstance(Utils.CONFIG);
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(object);
                    realm.commitTransaction();
                    realm.close();

                }else{
                    Utils.makeNetworkErrorToast(SchoolListActivity.this);
                    Utils.logdata("SATScoreNetworkCall Failed");
                }
            }

            @Override
            public void onFailure(Call<RealmList<SATData>> call, Throwable t) {
                Utils.makeNetworkErrorToast(SchoolListActivity.this);
                Utils.logdata("SATScoreNetworkCall Failed");
            }
        });

    }
    private void populateRecyclerView(List<SchoolData> response, @NonNull RecyclerView recyclerView) {
        try {
            recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, response, mTwoPane));
        }catch (Exception e){
            Utils.logException(e.getMessage());
        }
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final SchoolListActivity mParentActivity;
        private final List<SchoolData> mValues;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolData schoolData = (SchoolData) view.getTag();
                try {
                    if (mTwoPane) {
                        /**Creating the Fragment and loading it into the container on the same
                         * screen as this is in tablet mode*/
                        Bundle arguments = new Bundle();
                        arguments.putSerializable(SchoolDetailFragment.ARG, schoolData);
                        SchoolDetailFragment fragment = new SchoolDetailFragment();
                        fragment.setArguments(arguments);
                        mParentActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.school_detail_container, fragment)
                                .commit();
                    } else {
                        /**Creating the Activity which will create the Fragment and loading
                         * it into the container on the same screen as this is in narrow-screen mode*/
                        Context context = view.getContext();
                        Intent intent = new Intent(context, SchoolDetailActivity.class);
                        intent.putExtra(SchoolDetailFragment.ARG, schoolData);
                        context.startActivity(intent);
                    }
                }catch (Exception e){
                    Utils.logException(e.getMessage());
                }
            }
        };

        SimpleItemRecyclerViewAdapter(SchoolListActivity parent,
                                      List<SchoolData> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.school_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mIdView.setText(mValues.get(position).getSchoolName());
            holder.mContentView.setText(String.format("%s, %s", mValues.get(position).getCity(), mValues.get(position).getZip()));
            holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mIdView = (TextView) view.findViewById(R.id.tv_schoollistcontent_schoolname);
                mContentView = (TextView) view.findViewById(R.id.tv_schoollistcontent_location);
            }
        }
    }
}