package com.example.educatenyc.Network;

import com.example.educatenyc.Model.SATData;
import com.example.educatenyc.Model.SchoolData;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface NYCSchoolApi {
    @GET("s3k6-pzi2.json")
    Call<RealmList<SchoolData>> getListOfSchools();

//    @GET("s3k6-pzi2.json")
//    Call<List<SchoolData>> getSchools();

    @GET("f9bf-2cp4.json")
    Call<RealmList<SATData>> getSchoolsScores();
}
