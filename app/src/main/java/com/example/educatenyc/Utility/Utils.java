package com.example.educatenyc.Utility;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import io.realm.RealmConfiguration;

/** Utility class for repeatedly used methods*/
public class Utils {

    public static RealmConfiguration CONFIG;

    public static String addScores(String readingScore, String writingScore, String mathScore) {
            int intReadingScore = Integer.parseInt(readingScore);
            int intWritingScore = Integer.parseInt(writingScore);
            int intMathScore = Integer.parseInt(mathScore);
            return String.valueOf(intMathScore + intReadingScore + intWritingScore);
    }

    public static void logdata(String data) {
        Log.d("||",data);
    }

    public static void makeNetworkErrorToast(Activity activity) {
        Toast.makeText(activity, "Sorry...it's not you it's me!", Toast.LENGTH_SHORT).show();
    }

    public static void logException(String exceptionMessage) {
        Utils.logdata("Exception has occurred with message:"+exceptionMessage);
    }
}
