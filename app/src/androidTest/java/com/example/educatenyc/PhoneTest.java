package com.example.educatenyc;

import com.example.educatenyc.View.SchoolListActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSubstring;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class PhoneTest {

    private String validSchoolText;
    private String validSchoolTextWithLimitedInformation;
    private String invalidSchoolText;

    @Rule
    public ActivityTestRule<SchoolListActivity> activityRule = new ActivityTestRule<>(SchoolListActivity.class);

    @Before
    public void initValidString() {
        validSchoolText = "Liberation Diploma";
        validSchoolTextWithLimitedInformation = "Clinton School Writers";
        invalidSchoolText = "qqqq";
    }

    @Test
    public void schoolListRendering() throws InterruptedException {
        Thread.sleep(1500);
        onView(withId(R.id.rv_schoollist_schools)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    @Test
    public void toolbarQuery() throws InterruptedException {
        Thread.sleep(1500);
        onView(withId(R.id.sv_schoollist_search_bar)).perform(click()).perform(typeText(validSchoolText));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.rv_schoollist_schools)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withId(R.id.tv_schoollist_error_message)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }
    @Test
    public void toolbarFaultyQuery() throws InterruptedException {
        Thread.sleep(1500);
        onView(withId(R.id.sv_schoollist_search_bar)).perform(click()).perform(typeText(invalidSchoolText));
        onView(withId(R.id.tv_schoollist_error_message)).check(matches(withSubstring(invalidSchoolText)));
        Espresso.closeSoftKeyboard();
    }

    @Test
    public void schoolClick_SchoolDetailActivityOpens() throws InterruptedException {
        Thread.sleep(1500);
        onView(allOf(withId(R.id.tv_schoollistcontent_schoolname),withSubstring(validSchoolText))).perform(click());
        Thread.sleep(1500);
        onView(withId(R.id.tv_schooldetail_total_score)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

    }
    @Test
    public void schoolClick_SchoolDetailActivityOpens_NoInformation() throws InterruptedException {
        Thread.sleep(1500);
        onView(allOf(withId(R.id.tv_schoollistcontent_schoolname),withSubstring(validSchoolTextWithLimitedInformation))).perform(click());
        Thread.sleep(1500);
        onView(withId(R.id.tv_schooldetail_error_message)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    @Test
    public void schoolClick_SchoolDetailActivityOpens_BackClick() throws InterruptedException {
        Thread.sleep(1500);
        onView(allOf(withId(R.id.tv_schoollistcontent_schoolname),withSubstring(validSchoolTextWithLimitedInformation))).perform(click());
        Thread.sleep(1500);
        onView(withId(R.id.tv_schooldetail_error_message)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        Espresso.pressBack();
        onView(withId(R.id.rv_schoollist_schools)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

    }

}