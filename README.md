# 20200806-DeepanshPahwa-NYCSchools

## Documentation for the EdcuateNYC Android App
## Overview
This application shows the user all the schools in NYC. The user can search for certain schools using the search bar on top. When a user clicks on the schoo, they are shown the SAT performance stats for that school along with other academic opportunities.

This application uses the //___API. This API consists of information on everything NYC. However, in this application we are leveraging only the //school and //school_performance features of the API.
## Architecture
I have used a slightly unconventional approach for the application. Instead of making the network call for the school performance metrics whenthe user taps on school, the infromation is loaded and saved into a db when the application starts up. This allows for better perfromance. Additionally, this type of architecture opens doors to offline functionality of the app too. Since the information provided by this API endpoint is not changing rapidly(//better word), the saved data doesn't need to be updated frequently.

## Features
- The network call is made only when the application starts up. Navgating between activities in the app wont trigger a network call.
- Considering the architecture of the application, the Realm database used is indexed on "dbn" field which allows for faster querying and hence, faster performance.
- This application can be viewed on tablet also. There is a separate UI for both, handheld devices and tablet devices.
- The user can filter the list of schools if they are looking for a certain school by utilizing the search bar. There is an error message if there are no schools containing the keyword entered.
- Not all Schools have provided performance data. When the user clicks on such a school, there is an error page shown.
- In case of network error or any other error that results in the network cal failing, a toast will appear to signifies failed call.

## Additonal features that I would have added
- Instead of Using recyclerview to show the list of schools, use a library that allows sorting by name or location. One example would be https://github.com/ISchwarz23/SortableTableView
- I would like to show a progress bar before the data loads. (Can be implemented using RxJava)
- Currently, I am using Thread.sleep() in tests but I would like to use a better way to check if the data has been loaded into the recyclerview.
- I currently only have tests for handheld devices(narrow-screen). I would add additional tests for the app in tablet mode.
- I would functionality where user can filter schools based on location along with name.
- I would want to make the search not case-sensitive
- I would like to add the vanishing toolbar feature so that when user scrolls, the toolbar disappears.
