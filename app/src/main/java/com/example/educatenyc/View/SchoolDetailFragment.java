package com.example.educatenyc.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.educatenyc.Model.SATData;
import com.example.educatenyc.Model.SchoolData;
import com.example.educatenyc.R;
import com.example.educatenyc.Utility.Utils;

import androidx.fragment.app.Fragment;
import io.realm.Realm;

public class SchoolDetailFragment extends Fragment {

    public static final String ARG = "school_data";

    private SATData satData;
    private SchoolData schoolData;

    public SchoolDetailFragment() {
    }

    /**An instance of school data is passed as a part of the bundle. The "dbn" field is used to get the
     * corresponding performance data from the Realm database. For faster querying, the SATData class/table
     * is indexed on the "dbn" field */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG)) {
            Realm realm = Realm.getInstance(Utils.CONFIG);

            schoolData = (SchoolData) getArguments().getSerializable(ARG);
            satData = realm.where(SATData.class).equalTo(SATData.DBN, schoolData.getDbn()).findFirst();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.school_detail, container, false);

        /**Displaying the data for the school*/
        if (satData != null) {
            rootView.findViewById(R.id.school_detail_layout).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.ll_schooldetail_error_message_layout).setVisibility(View.GONE);

            String readingScore = satData.getSatCriticalReadingAvgScore();
            String writingScore = satData.getSatWritingAvgScore();
            String mathScore = satData.getSatMathAvgScore();

            String totalScore = Utils.addScores(readingScore, writingScore, mathScore);
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_school_name)).setText(satData.getSchoolName());
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_total_score)).setText(totalScore);
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_reading_score)).setText(readingScore);
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_writing_score)).setText(writingScore);
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_math_score)).setText(mathScore);
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_academic_opportunity_1)).setText(schoolData.getAcademicopportunities1());
            ((TextView) rootView.findViewById(R.id.tv_schooldetail_academic_opportunity_2)).setText(schoolData.getAcademicopportunities2());
        }else{
            rootView.findViewById(R.id.school_detail_layout).setVisibility(View.GONE);
            rootView.findViewById(R.id.ll_schooldetail_error_message_layout).setVisibility(View.VISIBLE);
        }
        return rootView;
    }
}